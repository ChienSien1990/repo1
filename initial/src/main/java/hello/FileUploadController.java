package hello;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.IOException;
import hello.FileUploadFileSystem;
import hello.FileUploadDBServlet;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;


@Controller
public class FileUploadController {

    private String AddMethod( MultipartFile file , String Name, String location){
        //Here is the add method to define whether the parameter is filesystem or server,
        // if it was filesystem, then it will create the instance of FileUploadFileSystem class
        // if it was server, then it will create the instanace of FileUploadDBServlet class
        if(location.equals("filesystem")){
            FileUploadFileSystem filesys = new FileUploadFileSystem();
            return filesys.handleFileUpload(Name,file);
        }
        else if(location.equals("server")) {
            FileUploadDBServlet DBserver = new FileUploadDBServlet();
            return DBServer.doPOST(HttpServletRequest request,HttpServletResponse response,Name)
        }
    }

    @RequestMapping(value="/upload", method=RequestMethod.GET)
    public @ResponseBody String provideUploadInfo() {
        return "You can upload a file by posting to this same URL.";
    }

    @RequestMapping(value="/upload", method=RequestMethod.POST)
    public @ResponseBody String handleFileUpload(@RequestParam("name") String name,
                                                 @RequestParam("file") MultipartFile file) {

        if(!file.isEmpty()){
            // input the third parameter as server or filesystem for choose the uploaded location
            // I inserted "server" as default
            return AddMethod(file,name,"server");
        }
        else{
            return "You failed to upload "+name+" because the file was empty.";
        }
    }
    }
