package hello;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;



public class FileUploadFileSystem {

    private String upmethod(  MultipartFile file , String name){
        InputStream is=null;
        BufferedInputStream bis = null;
        BufferedOutputStream stream= null;
        int c;
        try{

            is=file.getInputStream();
            bis=new BufferedInputStream(is);
            byte[] bytes=new byte[128];
            stream=new BufferedOutputStream(new FileOutputStream(new File(name)));
            while((c=bis.read(bytes))!=-1){
                stream.write(bytes,0,c);
            }
            return "You successfully uploaded " + name + "!";
        }
        catch(IOException e){
                return "You failed to upload "+name+" => "+e.getMessage();
            }
        finally{
                try {
                    is.close();
                    bis.close();
                    stream.close();
                }
                catch(IOException e){
                    return "You failed to upload "+name+" => "+e.getMessage();
                }
        }

    }


    public String provideUploadInfo() {
        return "You can upload a file by posting to this same URL.";
    }


    public String handleFileUpload( String name,
                                    MultipartFile file) {

        if(!file.isEmpty()){

            return upmethod(file,name);
        }
        else{
            return "You failed to upload "+name+" because the file was empty.";
        }
    }
    }
