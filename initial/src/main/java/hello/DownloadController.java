package hello;
import java.io.File;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;


import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 * Handles requests for the application home page.
 */
@Controller
public class DownloadController {

    private static final Logger logger = LoggerFactory
            .getLogger(DownloadController.class);

    @RequestMapping(value = "/donwloadfile", method = RequestMethod.GET)
    public String showform(Locale locale, Model model) {
        return "form";
    }

    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public void downloadnotepadfile(HttpServletRequest request,
                                    HttpServletResponse response) {

        try {
            String path = request.getSession().getServletContext()
                    .getRealPath("");
            File f = new File(path + "//" + "anote.doc");

            response.setContentType("application/txt");
            response.setContentLength(new Long(f.length()).intValue());
            response.setHeader("Content-Disposition",
                    "attachment; filename=sample.txt");
            FileCopyUtils.copy(new FileInputStream(f),
                    response.getOutputStream());

        } catch (Exception e) {

            e.printStackTrace();
        }

    }
}